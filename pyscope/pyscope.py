import os
import pygame
import time
import random
import RPi.GPIO as GPIO
import math
import urllib2
import datetime
import os.path
import PIL # sudo apt-get install python-imaging-tk
from PIL import Image

class pyscope :
    screen = None;
    
    def __init__(self):
        "Ininitializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print "I'm running under X display = {0}".format(disp_no)
        
        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except pygame.error:
                print 'Driver: {0} failed.'.format(driver)
                continue
            found = True
            break
    
        if not found:
            raise Exception('No suitable video driver found!')
        
        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print "Framebuffer size: %d x %d" % (size[0], size[1])
        self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        #self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN | pygame.NOFRAME)
        #self.screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
        # Clear the screen to start
        self.screen.fill((0, 0, 0))        
        # Initialise font support
        pygame.font.init()
        # Render the screen
        pygame.display.update()

    def __del__(self):
        "Destructor to make sure pygame shuts down, etc."

def weekDay(year, month, day):
    offset = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
    week   = {0:'Sunday', 
              1:'Monday', 
              2:'Tuesday', 
              3:'Wednesday', 
              4:'Thursday',  
              5:'Friday', 
              6:'Saturday'}
    afterFeb = 1
    if month > 2: afterFeb = 0
    aux = year - 1700 - afterFeb
    # dayOfWeek for 1700/1/1 = 5, Friday
    dayOfWeek  = 5
    # partial sum of days betweem current date and 1700/1/1
    dayOfWeek += (aux + afterFeb) * 365                  
    # leap year correction    
    dayOfWeek += aux / 4 - aux / 100 + (aux + 100) / 400     
    # sum monthly and day offsets
    dayOfWeek += offset[month - 1] + (day - 1)               
    dayOfWeek %= 7
    return dayOfWeek, week[dayOfWeek]

def date2filename(year, month, day):
    yymmdd = str(year-2000) + str(month).rjust(2, '0') + str(day).rjust(2, '0')
    
    dayofweek = weekDay(screentime.year, screentime.month, screentime.day)
    if dayofweek[0] > 0:  # not sunday
        filename = "tas" + yymmdd + ".gif"
    else:
        filename = "tas" + yymmdd + ".jpg"
        
    return filename

# setup pins as inputs for buttons
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN) # prev. day
GPIO.setup(24, GPIO.IN) # today
GPIO.setup(25, GPIO.IN) # next day

# Create an instance of the PyScope class
scope = pyscope()

# get screen size
screen_width = pygame.display.Info().current_w
screen_height = pygame.display.Info().current_h
#print "Screen width: %d" % (screen_width)

# hide mouse cursor
pygame.mouse.set_visible(False)

buttonpress = False

font = pygame.font.Font(None, 30)
textblack = False

day = datetime.timedelta(days=1)

screentime = datetime.date.today()

while True:
    inputValue_today = GPIO.input(24)
    inputValue_prev = GPIO.input(23)
    inputValue_next = GPIO.input(25)
    
    if (inputValue_today == True):
        print "today button pressed"
        
        screentime = datetime.date.today()  

        buttonpress = True
        
        time.sleep(.3)
        
    elif (inputValue_prev == True and inputValue_next == False):
        print "previous button pressed"

        screentime -= day
        
        buttonpress = True
        time.sleep(.3)
        
    elif (inputValue_next == True and inputValue_prev == False):
        print "next button pressed"
        
        screentime += day
        
        today = datetime.date.today()
        
        # if trying to advance past current day
        if screentime > today: 
            screentime = today
        else:    
            buttonpress = True
        
        time.sleep(.3)
        
    elif (inputValue_prev == True and inputValue_next == True):
        print "prev/next buttons pressed together - exiting"
        break
        
    if buttonpress == True:     # update image
        
        #print "year=%d month=%d day=%d" % (screentime_year-2000, screentime_month, screentime_day)
        print "year=%d month=%d day=%d" % (screentime.year-2000, screentime.month, screentime.day)
    
        filename = date2filename(screentime.year, screentime.month, screentime.day)
        print filename
        
        # check if file exists locally before downloading
        filename_base, extension = os.path.splitext(filename)
        filename_resize = filename_base + "_resize" + extension
        
        if os.path.isfile(filename_resize):  
            print "file already exists"
        else:
            try:
                picfile = urllib2.urlopen("http://images.ucomics.com/comics/tas/" + str(screentime.year) + "/" + filename) 
                
                output = open(filename,'wb')
                output.write(picfile.read())
                output.close()
                print "file downloaded"
                
                # resize image to fit screen
                # 300 x 353-364 (gif)
                # 600 x 293 (jpg)
                if os.path.splitext(filename)[1] == '.gif':
                    baseheight = screen_height
                    img = Image.open(filename)
                    hpercent = (baseheight/float(img.size[1]))
                    wsize = int((float(img.size[0])*float(hpercent)))
                    img = img.resize((wsize,baseheight), PIL.Image.ANTIALIAS)
                    img.save(filename_resize)
                else:                           # .jpg:
                    basewidth = screen_width
                    img = Image.open(filename)
                    wpercent = (basewidth/float(img.size[0]))
                    hsize = int((float(img.size[1])*float(wpercent)))
                    img = img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
                    img.save(filename_resize)                
                      
            except:
                filename_resize = 'ImgNotFound.jpg'
                print "file not found - will display error image"
                textblack = True
    
        # clear screen
        scope.screen.fill((0, 0, 0))

        logo = pygame.image.load(filename_resize).convert()
        surface_width = logo.get_width()
        surface_height = logo.get_height()
        print "Surface size: %d x %d\n" % (surface_width, surface_height)

        # center image/surface on screen
        scope.screen.blit(logo, (math.floor((screen_width-surface_width)/2), math.floor((screen_height-surface_height)/2)))
        
        # add date to corner of screen
        if textblack == False:
            text_surface = font.render('%d/%d/%d' % (screentime.month, screentime.day, screentime.year), True, (255, 255, 255)) # White text
            scope.screen.blit(text_surface, (0, 380))
        else:
            text_surface = font.render('%d/%d/%d' % (screentime.month, screentime.day, screentime.year), True, (0, 0, 0)) # Black text
            scope.screen.blit(text_surface, (370, 360))
            textblack = False
        
        pygame.display.update()
        
        buttonpress = False
        
    time.sleep(.01)

